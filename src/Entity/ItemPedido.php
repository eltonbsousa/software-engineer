<?php

namespace App\Entity;

use App\Repository\ItemPedidoRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ItemPedidoRepository::class)]
class ItemPedido
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Pedido::class, inversedBy: 'itemPedidos')]
    #[ORM\JoinColumn(nullable: false)]
    private $pedido;

    #[ORM\ManyToOne(targetEntity: Produto::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    private $produto;

    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    private $quantidade;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPedido(): ?Pedido
    {
        return $this->pedido;
    }

    public function setPedido(?Pedido $pedido): self
    {
        $this->pedido = $pedido;

        return $this;
    }

    public function getProduto(): ?Produto
    {
        return $this->produto;
    }

    public function setProduto(?Produto $produto): self
    {
        $this->produto = $produto;

        return $this;
    }

    public function getQuantidade(): ?int
    {
        return $this->quantidade;
    }

    public function setQuantidade(int $quantidade): self
    {
        $this->quantidade = $quantidade;

        return $this;
    }
}
