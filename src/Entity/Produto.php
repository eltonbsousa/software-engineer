<?php

namespace App\Entity;

use App\Repository\ProdutoRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProdutoRepository::class)]
class Produto
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 20)]
    #[Assert\NotBlank]
    private $codBarras;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private $nome;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 2)]
    #[Assert\NotBlank]
    private $valorUnitario;

    public function __toString(): string
    {
        $valorUnitarioFormatado = number_format($this->getValorUnitario(), 2, ',', '.');
        return "{$this->getCodBarras()} - {$this->getNome()} (R$ {$valorUnitarioFormatado})";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodBarras(): ?string
    {
        return $this->codBarras;
    }

    public function setCodBarras(string $codBarras): self
    {
        $this->codBarras = $codBarras;

        return $this;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(?string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getValorUnitario(): ?string
    {
        return $this->valorUnitario;
    }

    public function setValorUnitario(string $valorUnitario): self
    {
        $this->valorUnitario = $valorUnitario;

        return $this;
    }
}
