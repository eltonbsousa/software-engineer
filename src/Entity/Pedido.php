<?php

namespace App\Entity;

use App\Repository\PedidoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PedidoRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Pedido
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    private $numero;

    #[ORM\Column(type: 'datetime')]
    #[Assert\NotBlank]
    private $data;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 2)]
    private $total;

    #[ORM\OneToMany(mappedBy: 'pedido', targetEntity: ItemPedido::class, orphanRemoval: true)]
    #[Assert\Valid]
    private $itemPedidos;

    #[ORM\ManyToOne(targetEntity: Cliente::class, inversedBy: 'pedidos')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    private $cliente;

    public function __construct()
    {
        $this->itemPedidos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data = null): self
    {
        $this->data = $data;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return Collection|ItemPedido[]
     */
    public function getItemPedidos(): Collection
    {
        return $this->itemPedidos;
    }

    public function addItemPedido(ItemPedido $itemPedido): self
    {
        if (!$this->itemPedidos->contains($itemPedido)) {
            $this->itemPedidos[] = $itemPedido;
            $itemPedido->setPedido($this);
        }

        return $this;
    }

    public function removeItemPedido(ItemPedido $itemPedido): self
    {
        if ($this->itemPedidos->removeElement($itemPedido)) {
            // set the owning side to null (unless already changed)
            if ($itemPedido->getPedido() === $this) {
                $itemPedido->setPedido(null);
            }
        }

        return $this;
    }

    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    public function setCliente(?Cliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    #[ORM\PreFlush]
    public function calculaTotal(): void
    {
        $total = 0;

        foreach ($this->getItemPedidos() as $itemPedido) {
            $total += $itemPedido->getProduto()->getValorUnitario() * $itemPedido->getQuantidade();
        }

        $this->total = $total;
    }
}
