<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ValidatorException;

class CpfValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\Cpf */

        if (null === $value || '' === $value) {
            return;
        }

        try {
            self::validarCpf($value);
        } catch (ValidatorException $ex) {
            $this->context->buildViolation($ex->getMessage())
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }

    /**
     * Validar CPF.
     *
     * @param string $cpf
     *
     * @return bool
     * @throws ValidatorException
     */
    public static function validarCpf($cpf = null)
    {
        if (is_null($cpf)) {
            return false;
        }

        $cpf = preg_replace('/[^[:digit:]]/', '', $cpf);

        // Quantidade exata de dígitos
        if (11 !== strlen($cpf)) {
            throw new ValidatorException('O CPF deve ter 11 dígitos.');
        }

        if (!is_numeric($cpf)) {
            throw new ValidatorException('O campo deve ter somente números.');
        }

        switch ($cpf) {
            case 00000000000:
            case 11111111111:
            case 22222222222:
            case 33333333333:
            case 44444444444:
            case 55555555555:
            case 66666666666:
            case 77777777777:
            case 88888888888:
            case 99999999999:
                throw new ValidatorException('O formato deste CPF não é válido.');
        }

        $soma = 0;

        for ($i = 0; $i < 9; ++$i) {
            $soma += ((10 - $i) * $cpf[$i]);
        }

        $d1 = 11 - ($soma % 11);

        if ($d1 >= 10) {
            $d1 = 0;
        }

        $soma = 0;

        for ($i = 0; $i < 10; ++$i) {
            $soma += ((11 - $i) * $cpf[$i]);
        }

        $d2 = 11 - ($soma % 11);

        if ($d2 >= 10) {
            $d2 = 0;
        }

        if ($d1 != $cpf[9] || $d2 != $cpf[10]) {
            throw new ValidatorException('O formato deste CPF não é válido.');
        }

        return true;
    }
}
