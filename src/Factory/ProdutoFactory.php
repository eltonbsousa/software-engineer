<?php

namespace App\Factory;

use App\Entity\Produto;
use App\Repository\ProdutoRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Produto>
 *
 * @method static Produto|Proxy createOne(array $attributes = [])
 * @method static Produto[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Produto|Proxy find(object|array|mixed $criteria)
 * @method static Produto|Proxy findOrCreate(array $attributes)
 * @method static Produto|Proxy first(string $sortedField = 'id')
 * @method static Produto|Proxy last(string $sortedField = 'id')
 * @method static Produto|Proxy random(array $attributes = [])
 * @method static Produto|Proxy randomOrCreate(array $attributes = [])
 * @method static Produto[]|Proxy[] all()
 * @method static Produto[]|Proxy[] findBy(array $attributes)
 * @method static Produto[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Produto[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ProdutoRepository|RepositoryProxy repository()
 * @method Produto|Proxy create(array|callable $attributes = [])
 */
final class ProdutoFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            'codBarras' => self::faker()->ean13(),
            'nome' => ucwords(self::faker()->words(random_int(2, 5), true)),
            'valorUnitario' => self::faker()->randomFloat(2, 1, 100),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Produto $produto): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Produto::class;
    }
}
