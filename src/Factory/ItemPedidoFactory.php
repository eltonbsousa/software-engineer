<?php

namespace App\Factory;

use App\Entity\ItemPedido;
use App\Repository\ItemPedidoRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<ItemPedido>
 *
 * @method static ItemPedido|Proxy createOne(array $attributes = [])
 * @method static ItemPedido[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ItemPedido|Proxy find(object|array|mixed $criteria)
 * @method static ItemPedido|Proxy findOrCreate(array $attributes)
 * @method static ItemPedido|Proxy first(string $sortedField = 'id')
 * @method static ItemPedido|Proxy last(string $sortedField = 'id')
 * @method static ItemPedido|Proxy random(array $attributes = [])
 * @method static ItemPedido|Proxy randomOrCreate(array $attributes = [])
 * @method static ItemPedido[]|Proxy[] all()
 * @method static ItemPedido[]|Proxy[] findBy(array $attributes)
 * @method static ItemPedido[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ItemPedido[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemPedidoRepository|RepositoryProxy repository()
 * @method ItemPedido|Proxy create(array|callable $attributes = [])
 */
final class ItemPedidoFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            'pedido' => PedidoFactory::new(),
            'produto' => ProdutoFactory::new(),
            'quantidade' => self::faker()->numberBetween(1, 5),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(ItemPedido $itemPedido): void {})
        ;
    }

    protected static function getClass(): string
    {
        return ItemPedido::class;
    }
}
