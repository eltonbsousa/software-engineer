<?php

namespace App\Factory;

use App\Entity\Pedido;
use App\Repository\PedidoRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Pedido>
 *
 * @method static Pedido|Proxy createOne(array $attributes = [])
 * @method static Pedido[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Pedido|Proxy find(object|array|mixed $criteria)
 * @method static Pedido|Proxy findOrCreate(array $attributes)
 * @method static Pedido|Proxy first(string $sortedField = 'id')
 * @method static Pedido|Proxy last(string $sortedField = 'id')
 * @method static Pedido|Proxy random(array $attributes = [])
 * @method static Pedido|Proxy randomOrCreate(array $attributes = [])
 * @method static Pedido[]|Proxy[] all()
 * @method static Pedido[]|Proxy[] findBy(array $attributes)
 * @method static Pedido[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Pedido[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static PedidoRepository|RepositoryProxy repository()
 * @method Pedido|Proxy create(array|callable $attributes = [])
 */
final class PedidoFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            'cliente' => ClienteFactory::new(),
            'numero' => self::faker()->randomNumber(5),
            'data' => self::faker()->dateTimeBetween('-2 years', '-1 days'),
            'total' => self::faker()->randomFloat(2, 50, 100),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Pedido $pedido): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Pedido::class;
    }
}
