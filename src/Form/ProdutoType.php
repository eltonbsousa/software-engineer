<?php

namespace App\Form;

use App\Entity\Produto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProdutoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('codBarras', TextType::class, [
                'label' => 'Código de Barras',
            ])
            ->add('nome')
            ->add('valorUnitario', MoneyType::class, [
                'label' => 'Valor Unitário',
                'currency' => 'BRL'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Produto::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ]
        ]);
    }
}
