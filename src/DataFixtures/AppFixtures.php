<?php

namespace App\DataFixtures;

use App\Factory\ItemPedidoFactory;
use App\Factory\PedidoFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        PedidoFactory::createMany(50, [
            'itemPedidos' => ItemPedidoFactory::new()->many(1, 5),
        ]);
    }
}
