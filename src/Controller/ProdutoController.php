<?php

namespace App\Controller;

use App\Entity\ItemPedido;
use App\Entity\Produto;
use App\Form\ProdutoType;
use Doctrine\ORM\EntityManagerInterface;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\NumberColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/produto')]
class ProdutoController extends AbstractController
{
    #[Route('/', name: 'produto_index', methods: ['GET|POST'])]
    public function index(Request $request, DataTableFactory $dataTableFactory): Response
    {
        $table = $dataTableFactory->create()
            ->add('codBarras', TextColumn::class, [
                'label' => 'Código de Barras',
            ])
            ->add('nome', TextColumn::class, [
                'label' => 'Nome',
            ])
            ->add('valorUnitario', NumberColumn::class, [
                'label' => 'Valor Unitário (R$)',
                'render' => function ($value) {
                    return number_format($value, 2, ',', '.');
                },
            ])
            ->add('actions', TwigColumn::class, [
                'label' => 'Ações',
                'className' => 'app-table-row-actions',
                'template' => 'produto/_index_table_row_actions.html.twig',
            ])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Produto::class,
            ])
            ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('produto/index.html.twig', ['datatable' => $table]);
    }

    #[Route('/new', name: 'produto_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $produto = new Produto();
        $form = $this->createForm(ProdutoType::class, $produto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($produto);
            $entityManager->flush();

            return $this->redirectToRoute('produto_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('produto/new.html.twig', [
            'produto' => $produto,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'produto_show', methods: ['GET'])]
    public function show(Produto $produto): Response
    {
        return $this->render('produto/show.html.twig', [
            'produto' => $produto,
        ]);
    }

    #[Route('/{id}/edit', name: 'produto_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Produto $produto, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ProdutoType::class, $produto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('produto_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('produto/edit.html.twig', [
            'produto' => $produto,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'produto_delete', methods: ['POST'])]
    public function delete(Request $request, Produto $produto, EntityManagerInterface $entityManager): Response
    {
        if($this->canDelete($produto, $entityManager)) {
            if ($this->isCsrfTokenValid('delete' . $produto->getId(), $request->request->get('_token'))) {
                $entityManager->remove($produto);
                $entityManager->flush();

                $this->addFlash('success', 'Produto excluído com sucesso.');
            }
        } else {
            $this->addFlash('danger', 'Não foi possível realizar a exclusão, pois já existe Pedido com o Produto.');
        }

        return $this->redirectToRoute('produto_index', [], Response::HTTP_SEE_OTHER);
    }

    private function canDelete(Produto $produto, EntityManagerInterface $entityManager)
    {
        // Verifica se existe ItemPedido relacionado ao Produto
        $itemPedido = $entityManager->getRepository(ItemPedido::class)->findOneBy([
            'produto' => $produto
        ]);

        if (null !== $itemPedido) {
            return false;
        }

        return true;
    }
}
