<?php

namespace App\Controller;

use App\Entity\Cliente;
use App\Form\ClienteType;
use Doctrine\ORM\EntityManagerInterface;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/cliente')]
class ClienteController extends AbstractController
{
    #[Route('/', name: 'cliente_index', methods: ['GET|POST'])]
    public function index(Request $request, DataTableFactory $dataTableFactory): Response
    {
        $table = $dataTableFactory->create()
            ->add('nome', TextColumn::class, [
                'label' => 'Nome',
            ])
            ->add('cpf', TextColumn::class, [
                'label' => 'CPF',
            ])
            ->add('email', TextColumn::class, [
                'label' => 'Email',
            ])
            ->add('actions', TwigColumn::class, [
                'label' => 'Ações',
                'className' => 'app-table-row-actions',
                'template' => 'cliente/_index_table_row_actions.html.twig',
            ])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Cliente::class,
            ])
            ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('cliente/index.html.twig', ['datatable' => $table]);
    }

    #[Route('/new', name: 'cliente_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $cliente = new Cliente();
        $form = $this->createForm(ClienteType::class, $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($cliente);
            $entityManager->flush();

            return $this->redirectToRoute('cliente_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cliente/new.html.twig', [
            'cliente' => $cliente,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'cliente_show', methods: ['GET'])]
    public function show(Cliente $cliente): Response
    {
        return $this->render('cliente/show.html.twig', [
            'cliente' => $cliente,
        ]);
    }

    #[Route('/{id}/edit', name: 'cliente_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Cliente $cliente, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ClienteType::class, $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('cliente_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cliente/edit.html.twig', [
            'cliente' => $cliente,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'cliente_delete', methods: ['POST'])]
    public function delete(Request $request, Cliente $cliente, EntityManagerInterface $entityManager): Response
    {
        if ($this->canDelete($cliente)) {
            if ($this->isCsrfTokenValid('delete'.$cliente->getId(), $request->request->get('_token'))) {
                $entityManager->remove($cliente);
                $entityManager->flush();
            }

            $this->addFlash('success', 'Cliente excluído com sucesso.');
        } else {
            $this->addFlash('danger', 'Não foi possível realizar a exclusão, pois o Cliente já possui Pedido.');
        }

        return $this->redirectToRoute('cliente_index', [], Response::HTTP_SEE_OTHER);
    }

    private function canDelete(Cliente $cliente) {
        if (!$cliente->getPedidos()->isEmpty()) {
            return false;
        }

        return true;
    }
}
