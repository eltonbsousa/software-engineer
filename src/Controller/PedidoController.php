<?php

namespace App\Controller;

use App\Entity\Pedido;
use App\Form\PedidoType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Column\NumberColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/pedido')]
class PedidoController extends AbstractController
{
    #[Route('/', name: 'pedido_index', methods: ['GET|POST'])]
    public function index(Request $request, DataTableFactory $dataTableFactory): Response
    {
        $table = $dataTableFactory->create()
            ->add('cliente', TextColumn::class, [
                'label' => 'Cliente',
                'field' => 'cliente.nome',
            ])
            ->add('numero', NumberColumn::class, [
                'label' => 'Número do Pedido',
            ])
            ->add('data', DateTimeColumn::class, [
                'label' => 'Data',
                'format' => 'd/m/Y H:i',
                'searchable' => false,
            ])
            ->add('total', NumberColumn::class, [
                'label' => 'Total',
                'render' => function ($value) {
                    return number_format($value, 2, ',', '.');
                },
            ])
            ->add('actions', TwigColumn::class, [
                'label' => 'Ações',
                'className' => 'app-table-row-actions',
                'template' => 'pedido/_index_table_row_actions.html.twig',
            ])
            ->addOrderBy('data', DataTable::SORT_DESCENDING)
            ->createAdapter(ORMAdapter::class, [
                'entity' => Pedido::class,
            ])
            ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('pedido/index.html.twig', ['datatable' => $table]);
    }

    #[Route('/new', name: 'pedido_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $pedido = new Pedido();
        $form = $this->createForm(PedidoType::class, $pedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($pedido->getItemPedidos() as $itemPedido) {
                $itemPedido->setPedido($pedido);
                $entityManager->persist($itemPedido);
            }

            $entityManager->persist($pedido);
            $entityManager->flush();

            return $this->redirectToRoute('pedido_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pedido/new.html.twig', [
            'pedido' => $pedido,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'pedido_show', methods: ['GET'])]
    public function show(Pedido $pedido): Response
    {
        return $this->render('pedido/show.html.twig', [
            'pedido' => $pedido,
        ]);
    }

    #[Route('/{id}/edit', name: 'pedido_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Pedido $pedido, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PedidoType::class, $pedido);
        $form->handleRequest($request);

        $originalItens = new ArrayCollection();

        // Cria um ArrayCollection com os objetos ItemPedido atuais no banco
        foreach ($pedido->getItemPedidos() as $itemPedido) {
            $originalItens->add($itemPedido);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            // remove the relationship between the tag and the Task
            foreach ($originalItens as $itemPedido) {
                if (false === $pedido->getItemPedidos()->contains($itemPedido)) {
                    $itemPedido->setPedido(null);

                     $entityManager->remove($itemPedido);
                }
            }

            foreach ($pedido->getItemPedidos() as $itemPedido) {
                $itemPedido->setPedido($pedido);
                $entityManager->persist($itemPedido);
            }

            $entityManager->flush();

            return $this->redirectToRoute('pedido_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pedido/edit.html.twig', [
            'pedido' => $pedido,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'pedido_delete', methods: ['POST'])]
    public function delete(Request $request, Pedido $pedido, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pedido->getId(), $request->request->get('_token'))) {
            $entityManager->remove($pedido);
            $entityManager->flush();
        }

        $this->addFlash('success', 'Pedido excluído com sucesso.');

        return $this->redirectToRoute('pedido_index', [], Response::HTTP_SEE_OTHER);
    }
}
