## Instruções para executar o projeto

- Clonar o repositório e ir para o diretório
```
git clone https://eltonbsousa@bitbucket.org/eltonbsousa/software-engineer.git
cd software-engineer
```

- Instanciar container Docker
```
docker-compose build
docker-compose up -d
```

- Gerar assets
```
docker-compose exec php yarn install
docker-compose exec php yarn build
```

- Executar migrations e popular tabelas
```
docker-compose exec php bin/console doctrine:migrations:migrate
docker-compose exec php bin/console doctrine:fixtures:load
```

- Acessar `http:\\localhost`
